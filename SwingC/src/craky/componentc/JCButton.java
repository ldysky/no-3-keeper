package craky.componentc;

import java.awt.Color;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import craky.util.UIResourceManager;
import craky.util.UIUtil;

public class JCButton extends JButton
{
    private static final long serialVersionUID = -558139912606968740L;

    private Image image;

    private Image disabledImage;

    private Image pressedImage;

    private Image rolloverImage;
    
    private Image focusImage;
    
    private float alpha;
    
    private Insets imageInsets;
    
    private Insets focusImageInsets;
    
    private Color disabledTextColor;
    
    private boolean imageOnly;
    
    private boolean paintPressDown;
    
    public JCButton()
    {
        this(null, null);
    }
    
    public JCButton(Icon icon)
    {
        this(null, icon);
    }

    public JCButton(String text)
    {
        this(text, null);
    }
    
    public JCButton(Action action)
    {
        this(null, null);
        setAction(action);
    }
    
    public JCButton(String text, Icon icon)
    {
        super(text, icon);
        setUI(new CButtonUI());
        alpha = 1.0f;
        imageOnly = true;
        paintPressDown = true;
        imageInsets = new Insets(3, 3, 3, 3);
        focusImageInsets = new Insets(4, 4, 4, 4);
        disabledTextColor = new Color(103, 117, 127);
        setForeground(new Color(0, 28, 48));
        setBackground(Color.GRAY);
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setContentAreaFilled(false);
        setFont(UIUtil.getDefaultFont());
        setRolloverEnabled(true);
        setIconTextGap(5);
        setMargin(new Insets(0, 0, 0, 0));
        super.setOpaque(false);
        image = UIResourceManager.getImageByName("button_normal.png", true);
        disabledImage = UIResourceManager.getImageByName("button_disabled.png", true);
        pressedImage = UIResourceManager.getImageByName("button_pressed.png", true);
        rolloverImage = UIResourceManager.getImageByName("button_rollover.png", true);
        focusImage = UIResourceManager.getImageByName("button_foucs.png", true);
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
        this.repaint();
    }

    public Image getDisabledImage()
    {
        return disabledImage;
    }

    public void setDisabledImage(Image disabledImage)
    {
        this.disabledImage = disabledImage;
        this.repaint();
    }

    public Image getPressedImage()
    {
        return pressedImage;
    }

    public void setPressedImage(Image pressedImage)
    {
        this.pressedImage = pressedImage;
        this.repaint();
    }

    public Image getRolloverImage()
    {
        return rolloverImage;
    }

    public void setRolloverImage(Image rolloverImage)
    {
        this.rolloverImage = rolloverImage;
        this.repaint();
    }

    public Image getFocusImage()
    {
        return focusImage;
    }

    public void setFocusImage(Image focusImage)
    {
        this.focusImage = focusImage;
        this.repaint();
    }

    public float getAlpha()
    {
        return alpha;
    }

    public void setAlpha(float alpha)
    {
        if(alpha >= 0.0f && alpha <= 1.0f)
        {
            this.alpha = alpha;
            this.repaint();
        }
        else
        {
            throw new IllegalArgumentException("Invalid alpha:" + alpha);
        }
    }

    public Insets getImageInsets()
    {
        return imageInsets;
    }

    public void setImageInsets(int top, int left, int bottom, int right)
    {
        this.imageInsets.set(top, left, bottom, right);
        this.repaint();
    }

    public Insets getFocusImageInsets()
    {
        return focusImageInsets;
    }

    public void setFocusImageInsets(int top, int left, int bottom, int right)
    {
        this.focusImageInsets.set(top, left, bottom, right);
        this.repaint();
    }

    public boolean isPaintPressDown()
    {
        return paintPressDown;
    }

    public void setPaintPressDown(boolean paintPressDown)
    {
        this.paintPressDown = paintPressDown;
    }

    public Color getDisabledTextColor()
    {
        return disabledTextColor;
    }

    public void setDisabledTextColor(Color disabledTextColor)
    {
        this.disabledTextColor = disabledTextColor;
        
        if(!this.isEnabled())
        {
            this.repaint();
        }
    }

    public boolean isImageOnly()
    {
        return imageOnly;
    }

    public void setImageOnly(boolean imageOnly)
    {
        this.imageOnly = imageOnly;
        this.repaint();
    }
    
    @Deprecated
    public void updateUI()
    {}
    
    @Deprecated
    public void setOpaque(boolean opaque)
    {}
}