package craky.componentc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableModel;

import craky.component.JImagePane;
import craky.util.UIResourceManager;

public class JCScrollTable extends JCScrollPane implements ActionListener
{
    private static final long serialVersionUID = 4276053919432949896L;

    private static final Color DISABLED_BG = UIResourceManager.getColor(UIResourceManager.KEY_TEXT_DISABLED_BACKGROUND);
    
    public static final String COMMAND_SHOW_MENU = "ShowMenu";
    
    private Border border;
    
    private Border disabledBorder;
    
    private Color background;
    
    private JCTable table;
    
    private JCButton btnUpperRight;
    
    public JCScrollTable(JCTable table)
    {
        super();
        setViewportView(this.table = table);
        init();
    }
    
    public JCScrollTable()
    {
        this(new JCTable());
    }
    
    public JCScrollTable(TableModel dm)
    {
        this(new JCTable(dm));
    }
    
    public JCScrollTable(final Object[][] rowData, final Object[] columnNames)
    {
        this(new JCTable(rowData, columnNames));
    }
    
    public JCScrollTable(Vector<?> rowData, Vector<?> columnNames)
    {
        this(new JCTable(rowData, columnNames));
    }
    
    private void init()
    {
        setBorder(new LineBorder(new Color(84, 165, 213)));
        setDisabledBorder(new LineBorder(new Color(84, 165, 213, 128)));
        setBackground(UIResourceManager.getWhiteColor());
        setCorner(UPPER_RIGHT_CORNER, createUpperRightCorner());
        setHeaderDisabledForeground(new Color(123, 123, 122));
        table.setBorder(new EmptyBorder(0, 0, 0, 0));
        table.setDisabledBorder(table.getBorder());
        table.setVisibleInsets(0, 0, 0, 0);
        table.setAlpha(0.0f);
        table.setBackground(UIResourceManager.getEmptyColor());
        table.getTableHeader().setBackground(UIResourceManager.getEmptyColor());
    }
    
    protected void initHeader()
    {}
    
    private JComponent createUpperRightCorner()
    {
        final Image defaultImage = UIResourceManager.getImage(UIResourceManager.KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_IMAGE);
        JImagePane pane = new JImagePane();
        btnUpperRight = new JCButton();
        pane.setImageOnly(true);
        pane.setBorder(new EmptyBorder(0, 0, 1, 0));
        pane.setLayout(new BorderLayout());
        pane.setFilledBorderArea(false);
        pane.setMode(JImagePane.SCALED);
        pane.setImage(defaultImage);
        pane.setAlpha(0.5f);
        btnUpperRight.setImage(defaultImage);
        btnUpperRight.setRolloverImage(UIResourceManager.getImage(UIResourceManager.KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_ROLLOVER_IMAGE));
        btnUpperRight.setPressedImage(UIResourceManager.getImage(UIResourceManager.KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_PRESSED_IMAGE));
        btnUpperRight.setDisabledImage(btnUpperRight.getImage());
        btnUpperRight.setFocusable(false);
        btnUpperRight.setActionCommand(COMMAND_SHOW_MENU);
        btnUpperRight.addActionListener(this);
        pane.add(btnUpperRight);
        return pane;
    }
    
    public Border getDisabledBorder()
    {
        return disabledBorder;
    }

    public void setDisabledBorder(Border disabledBorder)
    {
        this.disabledBorder = disabledBorder;
        
        if(!this.isEnabled())
        {
            super.setBorder(disabledBorder);
        }
    }
    
    public void setBorder(Border border)
    {
        this.border = border;
        super.setBorder(border);
    }
    
    public Color getDisabledForeground()
    {
        return table.getDisabledForeground();
    }

    public void setDisabledForeground(Color disabledForeground)
    {
        table.setDisabledForeground(disabledForeground);
    }
    
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        super.setBorder(enabled? border: disabledBorder);
        super.setBackground(enabled? background: DISABLED_BG);
        btnUpperRight.setVisible(enabled);
    }
    
    public void setBackground(Color background)
    {
        this.background = background;
        super.setBackground(background);
    }
    
    public boolean isColumnControlEnabled()
    {
        return btnUpperRight.isEnabled();
    }

    public void setColumnControlEnabled(boolean enabled)
    {
        btnUpperRight.setEnabled(enabled);
    }
    
    public JCTable getTable()
    {
        return table;
    }
    
    @Deprecated
    public JLabel getHeaderLabel()
    {
        return null;
    }
    
    @Deprecated
    public HeaderPane getHeader()
    {
        return null;
    }
    
    @Deprecated
    public String getHeaderText()
    {
        return null;
    }
    
    @Deprecated
    public void setHeaderText(String text)
    {}
    
    public void actionPerformed(ActionEvent e)
    {
        if(e.getActionCommand().equals(COMMAND_SHOW_MENU))
        {
            JPopupMenu menu = table.getColumnControlMenu();

            if(menu != null && menu.getComponentCount() > 0)
            {
                Dimension buttonSize = btnUpperRight.getSize();
                menu.show(btnUpperRight, buttonSize.width - menu.getPreferredSize().width, buttonSize.height);
            }
        }
    }
    
    public void adjustmentValueChanged(AdjustmentEvent e)
    {}
}